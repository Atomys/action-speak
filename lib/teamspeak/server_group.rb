# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class ServerGroup < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor
    attr_reader   :sgid

    @@cache_list

    needed_permissions %i[b_virtualserver_servergroup_list]

    def initialize
      @connect_bypass = true
      super
    end

    def build(server_group_id)
      @@cache_list ||= Teamspeak::ActionSpeak.new.command('servergrouplist')
      raise Teamspeak.const_set('DatabaseEmptyResultSet', Class.new(StandardError)) unless (group = @@cache_list.select { |h| h['sgid'] == server_group_id }.first)
      group.each do |key, value|
        dyn_accessor(key, value)
      end

      self
    end

    def create(hash)
      hash.transform_keys!(&:to_sym).select! { |k, _| %i[name type].include?(k) }

      raise ArgumentError, 'Missing name param to create server group' unless hash[:name]

      server_group = Teamspeak::ActionSpeak.new.command('servergroupadd', hash)
      find(server_group['sgid'])
    end

    def destroy(force = 0)
      Teamspeak::ActionSpeak.new.command('servergroupdel', sgid: sgid, force: force)
    end

    def copy(target = 0, hash = {})
      hash.transform_keys!(&:to_sym).select! { |k, _| %i[name type].include?(k) }

      Teamspeak::ActionSpeak.new.command('servergroupcopy', { ssgid: sgid, tsgid: target }.merge(hash))['sgid']
    end

    def rename(name)
      update(name: name)
    end

    def perm_list
      Teamspeak::ActionSpeak.new.command('servergrouppermlist', sgid: sgid).map do |e|
        perm = Teamspeak::Permission.find(e['permid'])
        %w[value negated skip].each do |data|
          perm.send("#{data}=", e["perm#{data}"])
        end

        perm
      end
    end

    def add_perm(hash)
      check = %i[perm_id value negated skip]
      raise ArgumentError, "args don't include perm_id, value, negated or skip key" unless hash.transform_keys!(&:to_sym).keys & check == check

      Teamspeak::ActionSpeak.new.command('servergroupaddperm', sgid: sgid, permid: perm_id, permvalue: value, permnegated: negated, permskip: skip)
    end

    def add_perms(array)
      check = %i[perm_id value negated skip]
      raise ArgumentError, "An element of your array don't include perm_id, value, negated or skip key" unless array.map { |perm| perm.transform_keys!(&:to_sym).keys & check == check }.all?

      array.each { |perm| add_perm(perm) }
    end

    def del_perm(perm_id)
      Teamspeak::ActionSpeak.new.command('servergroupdelperm', sgid: sgid, permid: perm_id)
    end

    def del_perms(array)
      array.each { |perm| add_perm(perm) }
    end

    def raw_client_list(opts = %w[])
      opts.select! { |o| o.to_s == 'names' }

      Teamspeak::ActionSpeak.new.command('servergroupclientlist', { sgid: sgid }, opts)
    end

    def clients
      raw_client_list.map do |client|
        Teamspeak::Client.new.build(client['cldbid'])
      end
    end

    %w[add del].each do |action|
      define_method("#{action}_client") do |cldbid|
        Teamspeak::ActionSpeak.new.command("servergroup#{action}client", sgid: sgid, cldbid: cldbid)
      end
    end

    def self.list
      Teamspeak::ActionSpeak.new.command('servergrouplist').map do |e|
        Teamspeak::ServerGroup.find(e['sgid'])
      end
    end

    def self.find(server_group_id)
      Teamspeak::ServerGroup.new.build(server_group_id)
    end

    def self.find_by_client_id(client_id)
      Teamspeak::ActionSpeak.new.command('servergroupsbyclientid', cldbid: client_id).map do |e|
        Teamspeak::ServerGroup.find(e['sgid'])
      end
    end
  end
end
