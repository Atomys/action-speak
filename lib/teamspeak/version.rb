# frozen_string_literal: true

module Teamspeak
  # Displays the servers version information including platform and build number.
  class Version < ActionSpeak
    needed_permissions %i[b_serverinstance_version_view]

    def initialize
      super && command('version').each do |key, value|
        instance_variable_set("@#{key}", value)
        singleton_class.class_eval { attr_accessor key }
      end
    end
  end
end
