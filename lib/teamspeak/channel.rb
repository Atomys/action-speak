# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Channel < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor

    attr_reader   :id

    def build(channel_id)
      Teamspeak::ActionSpeak.new.command('channelinfo', cid: channel_id).each do |key, value|
        dyn_accessor(key.gsub(/^channel_/, ''), value)
      end

      self
    end

    def prepare_hash(hash = {})
      hash.reject! do |k, _|
        attr_readers.include?(k.to_sym)
      end

      hash.transform_keys! { |k| "channel_#{k}" }
    end

    def move(order, parent_id = 0)
      Teamspeak::ActionSpeak.new.command_and_disconnect('channelmove', cid: cid, cpid: parent_id, order: order)
    end

    def self.create(name, hash = {})
      as = new
      as.command_and_disconnect('channelcreate', { channel_name: name }.merge(as.prepare_hash(hash)))
      find_by_name(name)
    end

    def destroy(force = 0)
      Teamspeak::ActionSpeak.new.command('channeldelete', cid: cid, force: force)
    end
    alias :delete :destroy

    def save
      command('channeledit', respond_to?(:prepare_hash) ? prepare_hash(super) : super)
    end

    def self.list(opts = %w[])
      opts.map!(&:to_s).select! { |opt| %w[topic flags voice limits icon].include?(opt) }

      new.command('channellist', {}, opts).map do |e|
        find(e['cid'])
      end
    end

    def self.find(channel_id)
      new.build(channel_id)
    end

    def self.find_by_name(pattern)
      find(new.command_and_disconnect('channelfind', pattern: pattern)[0]['cid'])
    end

    def self.where_name_like(pattern)
      new.command_and_disconnect('channelfind', pattern: pattern).map do |c|
        find(c['cid'])
      end
    end
  end
end
