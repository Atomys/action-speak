# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Complain < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor

    @@cache_complaints = nil

    needed_permissions %i[
      b_client_complain_list
      i_client_complain_power i_client_needed_complain_power
      b_client_complain_delete b_client_complain_delete_own
    ]

    def initialize
      @connect_bypass = true
      super
    end

    def build(slug)
      @@cache_complaints ||= Teamspeak::ActionSpeak.new.command_and_disconnect('complainlist')
      from_client_dbid, timestamp = slug.split('-')

      return unless (res = @@cache_complaints.select { |h| h['timestamp'] == timestamp && h['fcldbid'] == from_client_dbid }.first)
      res.each do |key, value|
        dyn_accessor(key, value)
      end

      self
    end

    def self.create(target_client_dbid, message)
      Teamspeak::ActionSpeak.new.command_and_disconnect('complainadd', tcldbid: target_client_dbid, message: message)
      list(target_client_dbid).select { |complain| complain.message == message }.first
    end

    def delete
      Teamspeak::ActionSpeak.new.command_and_disconnect('complaindel', tcldbid: target_client_dbid, fcldbid: fcldbid)
    end

    def delete_for(target_client_dbid)
      Teamspeak::ActionSpeak.new.command_and_disconnect('complaindelall', tcldbid: target_client_dbid)
    end

    def self.list(target_client_dbid = nil)
      Teamspeak::ActionSpeak.new.command_and_disconnect('complainlist', target_client_dbid ? { tcldbid: target_client_dbid } : {}).map do |e|
        find("#{e['fcldbid']}-#{e['1523738827']}")
      end
    end

    def self.find(slug)
      new.build(slug)
    end
  end
end
