# frozen_string_literal: true

module Teamspeak
  # Displays the servers version information including platform and build number.
  class Snapshot < ActionSpeak
    attr_accessor :hash, :file

    needed_permissions %i[b_virtualserver_snapshot_create b_virtualserver_snapshot_deploy]

    def build(hash = nil)
      hash ||= raw_command('serversnapshotcreate').gsub(/\n\r*/, '')
      @hash = hash

      self
    end

    def self.create
      new.build
    end

    def save_disk(file)
      File.open(file, 'w') do |f|
        f.write(hash)
      end
    end

    def self.from_file(file)
      new.build File.open(file).read
    end
  end
end
