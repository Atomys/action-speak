# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Instance < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor

    attr_reader   :database_version, :permissions_version, :pending_connections_per_ip

    needed_permissions %i[b_serverinstance_info_view b_serverinstance_modify_settings]

    def initialize
      super && command('instanceinfo').each do |key, value|
        dyn_accessor(key.gsub(/^serverinstance_/, ''), value)
      end
    end

    def prepare_hash(hash = {})
      hash.reject! do |k, _|
        attr_readers.include?(k.to_sym)
      end

      hash.transform_keys! { |k| "serverinstance_#{k}" }
    end

    def save
      command('instanceedit', respond_to?(:prepare_hash) ? prepare_hash(super) : super)
    end
  end
end
