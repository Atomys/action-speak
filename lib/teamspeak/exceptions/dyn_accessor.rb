module Teamspeak
  module Exceptions
    module DynAccessor

      def dyn_accessor(attribute_name, attribute_value)
        unless self.respond_to?("#{attribute_name}=".to_sym)
          self.class.send(:define_method, "#{attribute_name}=".to_sym) do |value|
            instance_variable_set("@" + attribute_name.to_s, value)
          end
        end

        unless self.respond_to?(attribute_name.to_sym)
          self.class.send(:define_method, attribute_name.to_sym) do
            instance_variable_get("@" + attribute_name.to_s)
          end
        end

        self.send("#{attribute_name}=".to_sym, attribute_value)
      end
    end
  end
end