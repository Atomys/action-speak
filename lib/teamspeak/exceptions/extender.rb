module Teamspeak
  module Exceptions
    module Extender

      def e(klass)
        begin 
          klass.constantize
        rescue NameError
          Teamspeak.const_set(klass.sub('Teamspeak::', ''), Class.new(StandardError))
        end
      end
    end
  end
end