# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class ChannelGroup < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor
    
    attr_reader   :cgid

    @@cache_list

    needed_permissions %i[b_virtualserver_channelgroup_list]

    def initialize
      @connect_bypass = true
      super
    end

    def build(channel_group_id)
      @@cache_list ||= Teamspeak::ActionSpeak.new.command('channelgrouplist')
      @@cache_list.select { |h| h['cgid'] == channel_group_id }.first.each do |key, value|
        dyn_accessor(key, value)
      end

      self
    end

    def create(hash)
      hash.transform_keys!(&:to_sym).select! { |k, _| %i[name type].include?(k) }

      raise ArgumentError, 'Missing name param to create channel group' unless hash[:name]

      channel_group = Teamspeak::ActionSpeak.new.command('channelgroupadd', hash)
      find(channel_group['cgid'])
    end

    def destroy(force = 0)
      Teamspeak::ActionSpeak.new.command('channelgroupdel', cgid: cgid, force: force)
    end

    def copy(target = 0, hash = {})
      hash.transform_keys!(&:to_sym).select! { |k, _| %i[name type].include?(k) }

      Teamspeak::ActionSpeak.new.command('channelgroupcopy', { scgid: cgid, tcgid: target }.merge(hash))['cgid']
    end

    def rename(name)
      update(name: name)

      Teamspeak::ActionSpeak.new.command('channelgrouprename', { cgid: cgid, name: name })
    end

    def perm_list
      Teamspeak::ActionSpeak.new.command('channelgrouppermlist', cgid: cgid).map do |e|
        perm = Teamspeak::Permission.find(e['permid'])
        %w[value negated skip].each do |data|
          perm.send("#{data}=", e["perm#{data}"])
        end

        perm
      end
    end

    def add_perm(hash)
      check = %i[perm_id value negated skip]
      raise ArgumentError, "args don't include perm_id, value, negated or skip key" unless hash.transform_keys!(&:to_sym).keys & check == check

      Teamspeak::ActionSpeak.new.command('channelgroupaddperm', cgid: cgid, permid: perm_id, permvalue: value, permnegated: negated, permskip: skip)
    end

    def add_perms(array)
      check = %i[perm_id value negated skip]
      raise ArgumentError, "An element of your array don't include perm_id, value, negated or skip key" unless array.map { |perm| perm.transform_keys!(&:to_sym).keys & check == check }.all?

      array.each { |perm| add_perm(perm) }
    end

    def del_perm(perm_id)
      Teamspeak::ActionSpeak.new.command('channelgroupdelperm', cgid: cgid, permid: perm_id)
    end

    def del_perms(array)
      array.each { |perm| add_perm(perm) }
    end

    def raw_client_list(opts = %w[])
      opts.select! { |o| o.to_s == 'names' }

      Teamspeak::ActionSpeak.new.command('channelgroupclientlist', { cgid: cgid }, opts)
    end

    def clients
      raw_client_list.map do |client|
        Teamspeak::Client.new.build(client['cldbid'])
      end
    end

    def set_client_to_channel(cldbid, cid)
      Teamspeak::ActionSpeak.new.command("setclientchannelgroup", cgid: cgid, cldbid: cldbid, cid: cid)
    end

    def self.list
      Teamspeak::ActionSpeak.new.command('channelgrouplist').map do |e|
        Teamspeak::ChannelGroup.find(e['cgid'])
      end
    end

    def self.find(channel_group_id)
      Teamspeak::ChannelGroup.new.build(channel_group_id)
    end
  end
end
