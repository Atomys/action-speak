class String
  attr_accessor :acronyms, :acronyms_camelize_regex, :acronyms_underscore_regex

  def to_n
    num = index('.') ? to_f : to_i
    return nil unless num.to_s == self

    num
  end

  def n?
    to_n != nil
  end

  def acronym(word)
    (@acronyms ||= {})[word.downcase] = word
    define_acronym_regex_patterns

    self
  end

  def camelize(uppercase_first_letter = true)
    @acronyms = {} if @acronyms.nil?
    string = self
    if uppercase_first_letter
      string = string.sub(/^[a-z\d]*/) { @acronyms[$&] || $&.capitalize }
    else
      string = string.sub(/^(?:#{@acronym_regex}(?=\b|[A-Z_])|\w)/) { $&.downcase }
    end
    string.gsub!(/(?:_|(\/))([a-z\d]*)/) { "#{$1}#{@acronyms[$2] || $2.capitalize}" }
    string.gsub!(/\//, '::')
    string
  end

  def constantize
    Kernel.const_get(self)
  end

  private
  def define_acronym_regex_patterns
    @acronym_regex             = @acronyms.empty? ? /(?=a)b/ : /#{@acronyms.values.join("|")}/
    @acronyms_camelize_regex   = /^(?:#{@acronym_regex}(?=\b|[A-Z_])|\w)/
    @acronyms_underscore_regex = /(?:(?<=([A-Za-z\d]))|\b)(#{@acronym_regex})(?=\b|[^a-z])/
  end
end
